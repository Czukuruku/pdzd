import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Map.Entry;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.opencsv.CSVReader;



public class StubMapper extends Mapper<LongWritable, Text, Text, Text> {

	static HashMap<String, ArrayList<Double>> sport = new HashMap<String, ArrayList<Double>>();
	static HashMap<String, String> sportAvgBMIs = new HashMap<String, String>();
	
	static enum CountersEnum { INPUT_WORDS }
	static int weightColumnId = -1;
	static int heightColumnId = -1;
	static int sportColumnId = -1;
	static int eventColumnId = -1;
	
	@Override
	protected void cleanup(org.apache.hadoop.mapreduce.Mapper<LongWritable,Text,Text,Text>.Context context) throws IOException ,InterruptedException {
    	java.util.Iterator<Entry<String, ArrayList<Double>>> it = sport.entrySet().iterator();
    	double BMIaverage = 0;
    	while(it.hasNext()) {
    		//average BMI
    		Entry<String, ArrayList<Double>> pair = it.next();
            double sumBMI = 0;
            for (Double val : pair.getValue()) {
                sumBMI += val;
            }
            BMIaverage = sumBMI/(pair.getValue().size());
            
            sportAvgBMIs.put(pair.getKey(), BMIaverage+"");
    	}
	};
	
	@Override
	public void map(LongWritable key, Text value, Context context) 
	        throws IOException, InterruptedException {
	    
		String line = value.toString();
		CSVReader reader = new CSVReader(new StringReader(line));
        String [] tokens;
        Text word = new Text();
        
        tokens = reader.readNext();

        DoubleWritable BMI = new DoubleWritable();
        
        Text selectedLine = new Text();
        selectedLine.set(line);
        PlayerParameters playerParameters = new PlayerParameters();

        try {
        	for (int columnId = 0; columnId < tokens.length ; ++columnId) {
        		collectPlayerParameters(tokens, word, playerParameters, columnId);
        		

            }
            double BMIValue = playerParameters.weight/(Math.pow(playerParameters.height/100.0, 2));
//            BMI.set(BMIValue);
            
            
//            for (int columnId = 0; columnId < tokens.length ; ++columnId) {
            	collectBMI(BMIValue+"", tokens, word, sportColumnId);
//            }
            
            context.write(selectedLine, new Text(","+BMIValue));
        }
        catch(NumberFormatException e){
        	collectBMI("NA", tokens, word, sportColumnId);
        	context.write(selectedLine, new Text(",NA"));
        }

	
	    }

	private void collectBMI(String BMIValue, String[] tokens, Text word, int columnId) throws IOException, InterruptedException {
		String token = tokens[columnId];
		String event = tokens[eventColumnId];
		token = token.replace("\"", "");
		event = event.replace("\"", "");
		word.set(token+"|"+event);
		if(columnId == StubMapper.sportColumnId && !token.equals("Sport")) {
			if (!BMIValue.equals("NA"))
			{
				double currentBMI = Double.parseDouble(BMIValue);
		        if(sport.containsKey(word.toString())) {
		        	ArrayList<Double> currentArray = sport.get(word.toString());
		        	currentArray.add(currentBMI);
		        }
		        else {
		        	ArrayList<Double> newArray = new ArrayList<Double>();
		        	newArray.add(currentBMI);
		        	//generuj stringa
		        	sport.put(word.toString(), newArray);
		        }
			}                    
		}
	}

	private void collectPlayerParameters(String[] tokens, Text word,
			PlayerParameters playerParameters, int columnId) {
		String token = tokens[columnId];
		token = token.replace("\"", "");
		word.set(token);

		if(isFirstRow()){
		    if(token.equals("Height")) {
		        heightColumnId = columnId;
		    }
		    else if(token.equals("Weight")) {
		        weightColumnId = columnId;
		    }
		    else if(token.equals("Sport"))
		    {
		    	sportColumnId  = columnId;
		    }
		    else if(token.equals("Event")) {
		    	eventColumnId = columnId;
		    }
		}
		else {
		    if(columnId == heightColumnId) {
		        playerParameters.height = Integer.parseInt(word.toString());
		    }
		    else if(columnId == weightColumnId) {
		        playerParameters.weight = Integer.parseInt(word.toString());
		    }
		}
	}
	    private boolean isFirstRow() {
	        return ((weightColumnId == -1) || (heightColumnId == -1) || (sportColumnId == -1) || (eventColumnId == -1));
	    }
}