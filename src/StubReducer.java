import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.apache.commons.math3.util.MultidimensionalCounter.Iterator;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

import com.opencsv.CSVReader;

public class StubReducer extends Reducer<Text, Text, Text, Text> {
	
    static HashMap<String, ArrayList<Double>> sport = new HashMap<String, ArrayList<Double>>();
    
    @Override
    public void reduce(Text key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException {

            String line = key.toString();
            
            CSVReader reader = new CSVReader(new StringReader(line));
            String [] tokens;
            
            tokens = reader.readNext();
            for (int columnId = 0; columnId < tokens.length ; ++columnId) {
                String token = tokens[columnId];
                token = token.replace("\"", "");
        		String event = tokens[StubMapper.eventColumnId];
        		event = event.replace("\"", "");
                
                
                if(columnId == StubMapper.sportColumnId && !token.equals("Sport")) {
                	String currentValue = context.getCurrentValue().toString();
                	String disciplineBMI = StubMapper.sportAvgBMIs.get(token+"|"+event);
                	if(disciplineBMI == null){
                		String newValue = trimTabs(currentValue+",NA");
                		context.write(key, new Text(newValue));
                	}
                	else if(!disciplineBMI.equals("NA")) {
                		String newValue = trimTabs(currentValue+","+disciplineBMI);
                		context.write(key, new Text(newValue));
                	}
                	else
                	{
                		String newValue = trimTabs("NA,"+disciplineBMI);
                        context.write(key, new Text(newValue));
                	}
                }
            }

}

	private String trimTabs(String value) {
		String newValue = value;
		newValue = newValue.replace("\t", "");
		return newValue;
	}
}